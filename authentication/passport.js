var jwt = require('passport-jwt')
var JwtStrategy = jwt.Strategy,
    ExtractJwt = jwt.ExtractJwt;

var UserModel = require("../models/user");
var settings = require("./settings")

module.exports = function(passport) {
  var opts = {};

  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
  opts.secretOrKey = settings.jwt.secret

  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    UserModel.findById(jwt_payload._id, function(err, doc) {
        done(err, doc)
    })
  }));
};