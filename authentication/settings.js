module.exports = {
    database: {
        uri: 'mongodb://charlie:inchture00@ds217002.mlab.com:17002/fortnitefinder'
    },
    jwt: {
        secret: process.env.jwtSecret || '3C4F3799F12697AD612A1FE004CD83215E01EED82E3CF70ABC01809D97E7CBF92835C31715D6FBA603737C3EA010291506F59D9239FF50033D0374F40C9C134B'
    }
}