var express = require('express');
var router = express.Router();

var passport = require('passport');
require('../authentication/passport')(passport);

var settings = require("../authentication/settings")

var jwt = require('jsonwebtoken');
var UserModel = require("../models/user");
var request = require("request")

router.post('/login', function(req, res) {
    if(!req.body.email || !req.body.password || !req.body.token) {
        return res.send({success: false, msg: 'Please enter an email and password.'})
    }

    const verifyCaptchaOptions = {
        uri: "https://www.google.com/recaptcha/api/siteverify",
        json: true,
        form: {
            secret: process.env.CAPTCHA_SECRET || '6LcDfWkUAAAAAFzohhDUbz50de9LORrnZLX_tH6t',
            response: req.body.token
        }
    };

    request.post(verifyCaptchaOptions, function (err, response, body) {
        if (err) {
            console.log(err)
            return res.send({success: false, message: "Oops, something went wrong on our side"});
        }

        if (!body.success) {
            return res.send({success: false, message: "Invalid recaptcha!"});
        }

        UserModel.findOne({
            email: req.body.email
        }, function(err, user) {
            if (err) {
                res.send({success: false, msg: 'Authentication failed.'});
                return console.log(err)
            }
            if (!user) {
                res.send({success: false, msg: 'Authentication failed. User not found.'});
            } else {
                user.comparePassword(req.body.password, function (err, isMatch) {
                    if (isMatch && !err) {
                        var token = jwt.sign(user.toJSON(), settings.jwt.secret);
                        res.json({success: true, token: 'JWT ' + token, username: user.username});
                    } else {
                        res.send({success: false, msg: 'Authentication failed. Wrong password.'});
                    }
                });
            }
        });

    });

});

router.post('/register', function(req, res) {
    if (!req.body.username || !req.body.password || !req.body.email) {
        res.json({success: false, msg: 'Please make sure to include a email, username and password.'});
    } else {
        var newUser = new UserModel({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        });

        newUser.save(function(err) {
            if (err) {
                return res.json({success: false, msg: 'Username or email already exists.'});
            }
            res.json({success: true, msg: 'Successful created new user.'});
        });
    }
  });

module.exports = router;