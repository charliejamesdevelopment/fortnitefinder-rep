var express = require('express');
var router = express.Router();

var passport = require('passport');
require('../authentication/passport')(passport);

var ConversationModel = require("../models/conversation")
var MessageModel = require("../models/message")

router.get('/get', passport.authenticate('jwt', {session: false}), function(req, res) {
    ConversationModel.find({ $or:[{'author': req.user.username}, {'to': req.user.username}]}, null, {sort: '-updated'},  function(err, docs) {
        if(!err && docs) {
            res.send({success: true, conversations: docs})
        } else {
            res.send({success: false, message: "Something went wrong!"})
        }
    })
});

router.get('/notifications', passport.authenticate('jwt', {session: false}), function(req, res) {
    ConversationModel.find({ $or:[{'author': req.user.username}, {'to': req.user.username}], read: false}, function(err, docs) {
        if(!err && docs) {
            res.send({success: true, amount: docs.length})
        } else {
            res.send({success: false, message: "Something went wrong!"})
        }
    })
});

router.get('/get_specific/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
    ConversationModel.findById(req.params.id, function(err, doc) {
        if(!err && doc) {
            var conv = doc

            MessageModel.find({conversation: doc._id}).sort({"Date": -1}).limit(25).exec(function(err, messages) {
                if(!err && messages) {
                    res.send({success: true, conversation: conv, messages: messages})
                } else {
                    res.send({success: false, message: "Something went wrong while retrieving messages!"})
                }
            })
        } else {
            res.send({success: false, message: "Invalid conversation id!"})
        }
    })
})

var NotificationModel = require("../models/notification")

router.post('/c/add', passport.authenticate('jwt', {session: false}), function(req, res) {
    var id = req.body.id;
    var message = req.body.message
    if(!id || !message || id == '' || message == '') {
        return res.send({success: false, message: "Please enter a message!"});
    }
    ConversationModel.findById(id, function(err, doc) {
        if(!err && doc) {
            if(req.user.username === doc.to || req.user.username === doc.author) {
                var msg = new MessageModel({
                    message: message,
                    author: req.user.username,
                    conversation: doc._id
                })
                var noti = new NotificationModel({
                    message: "You have a new message from " + req.user.username,
                    to: req.user.username === doc.to ? doc.author : doc.to
                })

                noti.save()

                msg.save(function(err) {
                    if(!err) {
                        res.send({success: true})
                    } else {
                        console.log(err)
                        res.send({success: false, message: "Something went wrong."})
                    }
                })
            } else {
                res.send({success: false, message: "Unauthorized"})
            }
        } else {
            res.send({success: false, message: "Invalid conversation id!"})
        }
    })
});

router.post('/c/create', passport.authenticate('jwt', {session: false}), function(req, res) {
    var to = req.body.username;
    var from = req.user.username;
    var title = req.body.title;
    var message = req.body.message

    var cModel = new ConversationModel({
        title: title,
        initial_message: message,
        to: to,
        author: from,
    })

    cModel.save(function(err, item) {
        if(!err && item) {
            var notification = new NotificationModel({
                to: item.to,
                message: "You have a new message from " + item.author + "!"
            })

            notification.save()
            res.send({success: true})
        } else {
            console.log(err)
            res.send({success: false, message: "Something went wrong!"})
        }
    })
});

module.exports = router;