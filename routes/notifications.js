var express = require('express')
var router = express.Router();

var passport = require('passport');
require('../authentication/passport')(passport);

var NotificationModel = require("../models/notification")

router.get('/get/:del', passport.authenticate('jwt', {session: false}), function(req, res) {
    NotificationModel.find({to: req.user.username, read: false}, null, {sort: '-created'},  function(err, docs) {
        if(!err && docs) {
            if(req.params.del == "true") {
                NotificationModel.updateMany({to: req.user.username}, {read: true}, function(err) {
                    console.log(err)
                })
            }
            res.send({success: true, notifications: docs})
        } else {
            res.send({success: false, message: "Something went wrong!"})
        }
    })
});

module.exports = router;