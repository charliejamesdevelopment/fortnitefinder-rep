var express = require('express')
var router = express.Router();

var passport = require('passport')
require('../authentication/passport')(passport);

/*

{
  author: {
    account_name: { type: String, required: true },
    _id: { type: String, required: true },
    username: { type: String, required: true },
  },
  description: { type: String, required: true, unique: true },
  level: { type: String, required: true },
  type: { type: String, required: true },
  region: { type: String, required: true },
  voice_chat: { type: String, required: true },
  platform: { type: String, required: true },
  posted: Date
}

*/

var PostModel = require("../models/post")

function clean(obj) {
  for (var propName in obj) { 
    if (obj[propName] === null || obj[propName] === undefined || obj[propName] === '') {
      delete obj[propName];
    }
  }
  return obj
}

router.post('/search', function(req, res) {
  var level = undefined
  var limit = req.body.limit
  if(req.body.query.level != 'Any' && req.body.query.level) {
    level = { $gt: req.body.query.level - 1 }
  }
  var opts = clean({
    voice_chat: req.body.query.voice_chat,
    platform: req.body.query.platform,
    region: req.body.query.region,
    type: req.body.query.type,
    level: level
  })

  if(limit) {
    PostModel.find(opts).sort({'posted': -1}).limit(parseInt(limit)).exec(function(err, docs) {
      if(!err && docs) {
        res.send({success: true, items: docs})
      } else {
        console.log(err)
        res.send({success: false, message: "Something went wrong."})
      }
    })
  } else {
    PostModel.find(opts, null, {sort: '-posted'}, function(err, docs) {
      if(!err && docs) {
        res.send({success: true, items: docs})
      } else {
        console.log(err)
        res.send({success: false, message: "Something went wrong."})
      }
    })
  }
})

router.get('/get/:id', function(req, res) {
  PostModel.findById(req.params.id, function(err, doc) {
    if(!err && doc) {
      res.send({success: true, item: doc})
    } else {
      res.send({success: false, message: "Invalid post id."})
    }
  })
})

router.get('/delete/:id', passport.authenticate('jwt', {session: false}), function(req, res) {
  PostModel.findById(req.params.id, function(err, doc) {
    if(!err && doc) {
      if(doc.author.username === req.user.username) {
        PostModel.remove({_id: doc._id}, function(err) {
          if(!err) {
            res.send({success: true, message: "Deleted!"})
          } else {
            res.send({success: false, message: "Something went wrong!"})
          }
        })
      } else {
        res.send({success: false, message: "You cannot perform this action!"})
      }
    } else {
      res.send({success: false, message: "Invalid post id."})
    }
  })
})

router.post('/create', passport.authenticate('jwt', {session: false}), function(req, res) {
  if(req.body.description.length > 75) {
    return res.send({success: false, message: "Please make sure your description is no longer than 75 characters long."})
  }

  if(req.body.level > 100 || req.body.level < 0) {
    return res.send({success: false, message: "Please enter a level between 0 and 100, thanks."})
  }
  var post = new PostModel({
    author: {
      account_name: req.body.account_name,
      _id: req.user._id,
      username: req.user.username
    },
    description: req.body.description,
    level: req.body.level,
    type: req.body.type,
    region: req.body.region,
    voice_chat: req.body.voice_chat,
    platform: req.body.platform
  })

  post.save(function(err, newPost) {
    if(!err && newPost) {
      res.send({success: true})
    } else {
      console.log(err)
      res.send({success: false, message: "Something went wrong, did you enter all the fields correctly?"})
    }
  })
});

module.exports = router;