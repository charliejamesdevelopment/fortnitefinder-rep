var createError = require('http-errors');
var express = require('express');
var cors = require('cors')
var http = require('http');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var passport = require("passport")
require('./authentication/passport')(passport);

var settings = require("./authentication/settings")

var app = express();

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.connect(settings.database.uri, {
  promiseLibrary: require('bluebird'), 
  useNewUrlParser: true }
).then(() => { 
  console.log('Connection to mongo database successful!') 
}).catch((err) => {
  console.log("Failed to connect to the mongo database.")
  console.error(err)
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize()) 
app.use(cors())

var posts = require('./routes/posts');
var authentication = require('./routes/auth');
var messages = require('./routes/messages');
var notifications = require('./routes/notifications');

app.use('/api/v1/lfg', posts);
app.use('/api/v1/auth', authentication);
app.use('/api/v1/messages', messages);
app.use('/api/v1/notifications', notifications);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error')
});

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

var server = http.createServer(app);
var debug = require('debug')('api:server');

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}


module.exports = app;
