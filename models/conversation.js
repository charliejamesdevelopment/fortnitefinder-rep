var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var conversationSchema = new Schema({
  title: {type: String, required: true},
  initial_message: {type: String, required: true},
  author: {type: String, required: true},
  to: {type: String, required: true},
  updated: Date,
  created: Date
}, {
    collection: 'conversations'
});

conversationSchema.pre('save', function(next) {
    var currentDate = new Date();

    this.updated = currentDate;

    if (!this.created) {
        this.created = currentDate;
    }

    next();
});

var Conversation = mongoose.model('Conversation', conversationSchema);

module.exports = Conversation;