var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var messageSchema = new Schema({
  conversation: {type: String, required: true},
  message: {type: String, required: true},
  author: {type: String, required: true},
  date: Date
}, {
    collection: 'messages'
});

messageSchema.pre('save', function(next) {
    var currentDate = new Date();

    if (!this.created) {
        this.date = currentDate;
    }

    next();
});

var Message = mongoose.model('Message', messageSchema);

module.exports = Message;