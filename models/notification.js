var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notificationSchema = new Schema({
  message: {type: String, required: true},
  to: {type: String, required: true},
  created: Date,
  read: {type: Boolean, required: true, default: false}
}, {
    collection: 'notifications'
});

notificationSchema.pre('save', function(next) {
    var currentDate = new Date();

    if (!this.created) {
        this.created = currentDate;
    }

    next();
});

var Notification = mongoose.model('Notification', notificationSchema);

module.exports = Notification;