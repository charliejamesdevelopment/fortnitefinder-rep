var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;

var postSchema = new Schema({
  author: {
    account_name: { type: String, required: true },
    _id: { type: String, required: true },
    username: { type: String, required: true },
  },
  description: { type: String, required: true, unique: true },
  level: { type: Number, required: true },
  type: { type: String, required: true },
  region: { type: String, required: true },
  voice_chat: { type: String, required: true },
  platform: { type: String, required: true },
  posted: Date
}, {
    collection: 'posts'
});

/*{
    _id: 423423231444,
    author: {
        account_name: "mcunicorn",
        _id: 2344,
        username: "jayditum"
    },
    description: "Need a 4th for professional gb's and scrims, hmu",
    level: "75+",
    type: "Battle Royale",
    region: "SA",
    voice_chat: "Discord",
    platform: "PC",
    posted: Date.now()
}*/

postSchema.pre('save', function(next) {
    var currentDate = new Date();

    this.posted = currentDate;

    next();
});

var Post = mongoose.model('Post', postSchema);

module.exports = Post;